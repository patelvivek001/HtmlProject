var vendorName;
var vendorId;
$(document).ready(function() {
	connectDB('OnlineShopping');
	vendorId= getParameterByName("id");
	bindPage(vendorId);
	$("#bussinesHrs").attr('href','businessHours.html?id='+vendorId);

});

function connectDB(dbName) {
	db = openDatabase(dbName, '1.0', 'OnlineShopping', 2 * 1024 * 1024);
}

function bindPage(vendorId) {
	db.transaction(function(tx) {
		tx.executeSql('Select * from VENDOR where ID = ' + vendorId + ';', [], function(tx, results) {
			vendorName = results.rows[0].VENDOR;
			$('#vendorName').html(results.rows[0].VENDOR);
			$('#vendorAddress').html(results.rows[0].ADDRESS);
			$('#vendorPhone').html(results.rows[0].PHONENUMBER);
			$('#vendorImage').attr('src', '../images/' + results.rows[0].IMAGEPATH);
		});
	});

	db.transaction(function(tx) {
		tx.executeSql('SELECT * FROM PRODUCT Where vendor = "' + vendorName + '";', [], function(tx, results) {
			for (var i = 0; i < results.rows.length; i++) {
				$("#ulProduct").append('<li id="' + results.rows[i].pname + '">' + '<a id="' + results.rows[i].id + '"   href ="productDetail.html?id=' + results.rows[i].id + '&vendorId=' +vendorId+ '" target=\'_self\'><img style="height: 80%;width: 54px;" src= "../images/' + results.rows[i].image + '">' + '<div>' + ' <strong>' + results.rows[i].pname + '</strong><br />' + ' <strong> ' + results.rows[i].desc + '</div>' + ' </a>' + '</li>');
			}
			$("#ulProduct").listview('refresh');

		});
	});
}

function getParameterByName(name) {
	if (!url)
		var url = window.location.href;
	var name = name.replace(/[]/g, "\\$&");
	var regex = new RegExp("[?&]" + name + "(=([^]*)|&|#|$)"), results = regex.exec(url);
	if (!results)
		return null;
	if (!results[2])
		return '';

	return decodeURIComponent(results[2].replace(/\+/g, " "));
}
