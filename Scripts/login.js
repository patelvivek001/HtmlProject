var db;
var urlEmaill;
var txtEmail;
var txtPwd;

$(document).ready(function() {
	connectDB('OnlineShopping');
	$("#lblErrEmail").hide();
	urlEmail = getParameterByName("username");

	if (urlEmail != null)
		$("#txtEmail").val(urlEmail);
});

function connectDB(dbName) {
	db = openDatabase(dbName, '1.0', 'OnlineShopping', 2 * 1024 * 1024);
}

function validate() {
	txtEmail = $("#txtEmail").val();
	txtPwd = $("#txtPass").val();
	$("#spnEmail").hide();
	$("#spnPAss").hide();
	if (txtEmail == "" || txtPwd == "") {
		if(txtEmail == "" ){
			$("#spnEmail").show();
		}
		if(txtPwd == ""){
			$("#spnPAss").show();
		}
	} else {
		var dbPwd = db.transaction(function(tx) {
			tx.executeSql('Select * FROM USER where email = "' + txtEmail + '" ', [], function(tx, results) {
				if (results.rows.length > 0) {
					var isAdmin = false;
					if(txtEmail == "admin@admin.com")
						isAdmin = true;
					
					if (txtPwd == results.rows["0"].password) {
						localStorage.setItem("emailId", txtEmail);
						localStorage.setItem("userId", results.rows["0"].id);
						redirectPage('home.html?isAdmin='+isAdmin + '&username=' + txtEmail);
					} else {
						$("#lblErrEmail").show();
					}
				} else {
					$("#lblErrEmail").show();
				}
			});
		});
	}
}

function redirectPage(pageName) {
	window.location.href = pageName;
}

function getParameterByName(name) {
	if (!url)
		var url = window.location.href;
	var name = name.replace(/[]/g, "\\$&");
	var regex = new RegExp("[?&]" + name + "(=([^]*)|&|#|$)"), results = regex.exec(url);
	if (!results)
		return null;
	if (!results[2])
		return '';

	return decodeURIComponent(results[2].replace(/\+/g, " "));
}
