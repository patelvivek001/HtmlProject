var db;
var filePath;

$(document).ready(function() {
	connectDB('OnlineShopping');
	$('input[type="file"]').change(function(e) {
		filePath = e.target.files[0].name;
	});
});

function connectDB(dbName) {
	db = openDatabase(dbName, '1.0', 'OnlineShopping', 2 * 1024 * 1024);
}

function btnSave() {

	db.transaction(function(tx) {
		tx.executeSql('CREATE TABLE IF NOT EXISTS VENDOR(ID INTEGER PRIMARY KEY,VENDOR,ADDRESS,PHONENUMBER,IMAGEPATH,MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY,SATURDAY,SUNDAY)');
	});

	var vendorName = $('#txtVname').val();
	var address = $('#txtAddress').val();
	var phonenumberValue = $('#txtPhone').val();
	var imagepathValue = filePath;
	var mondayValue = $('#txtMon').val();
	var tuesdayValue = $('#txtTue').val();
	var wednesdayValue = $('#txtWed').val();
	var thursdayValue = $('#txtThu').val();
	var fridayValue = $('#txtFri').val();
	var saturdayValue = $('#txtSat').val();
	var sundayValue = $('#txtSun').val();
	var isAdmin = false;

	if (vendorName == "" || address == "" || phonenumberValue == "" || imagepathValue == "" || mondayValue == "" || tuesdayValue == "" || wednesdayValue == "" || thursdayValue == "" || fridayValue == "" || saturdayValue == "" || sundayValue == "") {
		alert("Please Enter all fields");
	} else {
		isAdmin = true;
		db.transaction(function(tx) {
			tx.executeSql('INSERT INTO VENDOR(VENDOR,ADDRESS,PHONENUMBER,IMAGEPATH,MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY,SATURDAY,SUNDAY) VALUES ("' + vendorName + '","' + address + '","' + phonenumberValue + '","' + imagepathValue + '","' + mondayValue + '","' + tuesdayValue + '","' + wednesdayValue + '","' + thursdayValue + '","' + fridayValue + '","' + saturdayValue + '","' + sundayValue + '")');
		});
		redirectPage('home.html?isAdmin='+isAdmin);

	}
}


function redirectPage(pageName) {
	window.location.href = pageName;
}