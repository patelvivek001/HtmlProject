function btnSignup() {
	var db = openDatabase('OnlineShopping', '1.0', 'OnlineShopping', 2 * 1024 * 1024);

	db.transaction(function(tx) {
		tx.executeSql('CREATE TABLE IF NOT EXISTS USER  (id INTEGER  PRIMARY KEY ,name,email,password,address,phonenumber)');
	});

	var txtName = $("#txtName").val();
	var txtEmail = $("#txtEmailID").val();
	var txtPwd = $("#txtPassword").val();
	var txtAreaAddress = $("#txtAddress").val();
	var txtPhone = $("#txtPhone").val();

	if (validate() && validateEmail(txtEmail)) {
		db.transaction(function(tx) {
			tx.executeSql('INSERT INTO USER (name,email,password,address,phonenumber) VALUES ("' + txtName + '","' + txtEmail + '","' + txtPwd + '","' + txtAreaAddress + '","' + txtPhone + '")');
		});
		document.location.replace('login.html?username=' + txtEmail);
	}
}

function validate() {
	var txtName = $("#txtName").val();
	var txtEmail = $("#txtEmailID").val();
	var txtPwd = $("#txtPassword").val();
	var txtAreaAddress = $("#txtAddress").val();
	var txtPhone = $("#txtPhone").val();

	//var errorMsg = "";
	
	//$( "#divError" ).empty();
	$("#spnName").hide();
	$("#spnEmail").hide();
	$("#spnPAss").hide();
	$("#spnAdd").hide();
	$("#spnNo").hide();
	if (txtName == "" || txtEmail == "" || txtPwd == "" || txtAreaAddress == "" || txtPhone == "") {
		if (txtName == "") {
			$("#spnName").show();
			//errorMsg += "Please enter Name <br>";
		}
		if (txtEmail == "")
			$("#spnEmail").show();
			//errorMsg += "Please enter Email <br>";
		if (txtPwd == "")
			$("#spnPAss").show();
			//errorMsg += "Please enter Password <br>";
		if (txtAreaAddress == "")
			$("#spnAdd").show();
			//errorMsg += "Please enter Address <br>";
		if (txtPhone == "")
			$("#spnNo").show();
			//errorMsg += "Please enter Phone Number <br>";
		//$('#divError').append(errorMsg);
		return false;
	} else {
		return true;
	}
}

function validateEmail(txtEmail) {
	var atpos = txtEmail.indexOf("@");
	var dotpos = txtEmail.lastIndexOf(".");
	if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= txtEmail.length) {
		var errorMsg = "Enter Valid E-mail address";
		$('#divError').append(errorMsg);
		return false;
	} else
		return true;
}

