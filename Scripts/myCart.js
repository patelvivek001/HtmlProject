var db;
$(document).ready(function() {
	connectDB('OnlineShopping');
	bindPage();
});

function connectDB(dbName) {
	db = openDatabase(dbName, '1.0', 'OnlineShopping', 2 * 1024 * 1024);
}

function bindPage() {
	var id = "";
	db.transaction(function(tx) {
		tx.executeSql('select * from CART where userId = "' + localStorage.getItem("userId") + '";', [], function(tx, result) {
			for (var j = 0; j < result.rows.length; j++) {
				Oneid = result.rows[j].productId + ",";
				id = id + Oneid;
			}
			id = id.slice(0, -1);
		});
	});

	db.transaction(function(tx) {
		tx.executeSql('SELECT * FROM PRODUCT Where ID in (' + id + ' );', [], function(tx, results) {
			for (var i = 0; i < results.rows.length; i++) {
				$("#ulProduct").append('<li id="' + results.rows[i].pname + '">' + '<a onclick = "deleteCart(' + results.rows[i].id + ');" id="' + results.rows[i].id + '"  target=\'_self\'><img style="height: 80%;width: 54px;" src= "../images/' + results.rows[i].image + '">' + '<div>' + ' <strong>' + results.rows[i].pname + '</strong><br />' + ' <strong> ' + results.rows[i].desc + '</div>' + ' </a>' + '</li>');
			}
			$("#ulProduct").listview('refresh');

		});
	});
}

function deleteCart(pid) {
	db.transaction(function(tx) {
		tx.executeSql('DELETE FROM CART Where productId ="' + pid + '";');
	});
	redirectPage('myCart.html');
}

function redirectPage(pageName) {
	window.location.href = pageName;
}