var db;
var userName;
$(document).ready(function() {
	connectDB('OnlineShopping');
	var isAdmin = getParameterByName("isAdmin");
	userName =  localStorage.getItem("emailId");
	if (isAdmin == "false" || userName != "admin@admin.com" )
		hideButtons();

	bindVendors();

});

function connectDB(dbName) {
	db = openDatabase(dbName, '1.0', 'OnlineShopping', 2 * 1024 * 1024);
	db.transaction(function(tx) {
		tx.executeSql('CREATE TABLE IF NOT EXISTS CART(ID INTEGER PRIMARY KEY,userId,productId)');
	});

}

function bindVendors() {
	db.transaction(function(tx) {
		tx.executeSql('Select * from VENDOR', [], function(tx, results) {

			for (var i = 0; i < results.rows.length; i++) {
				$("#ulList").append('<li id="' + results.rows[i].VENDOR + '">' + '<a id="' + results.rows[i].ID + '"   href ="ProductList.html?id=' + results.rows[i].ID + '" target=\'_self\'><img style="height: 80%;width: 54px;" src= "../images/' + results.rows[i].IMAGEPATH + '">' + '<div>' + ' <strong>' + results.rows[i].VENDOR + '</strong><br />' + ' <strong> ' + results.rows[i].ADDRESS + '<br /> Telephone : ' + results.rows[i].PHONENUMBER + '</strong>' + '</div>' + ' </a>' + '</li>');
			}

			$("#ulList").listview('refresh');
		});
	});
}

function hideButtons() {
	$("#btnAddProduct").hide();
	$("#btnAddVendor").hide();
}

function getParameterByName(param) {
	var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
	for (var i = 0; i < url.length; i++) {
		var urlparam = url[i].split('=');
		if (urlparam[0] == param) {
			return urlparam[1];
		}
	}
}

function btnAddProduct() {
	redirectPage('AddProduct.html');
}

function btnAddVendor() {
	redirectPage('AddVendor.html');
}

function redirectPage(pageName) {
	window.location.href = pageName;
}

function MyProf() {
	redirectPage('MyProfile.html?username=' + userName);
}