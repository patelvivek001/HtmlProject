var vendorName;
var vendorId;
var productId;
$(document).ready(function() {
	connectDB('OnlineShopping');
	productId = getParameterByName("id");
	vendorId = getParameterByName("vendorId");
	bindPage(productId);
	$("#aProduct").attr('href','ProductList.html?id='+vendorId);

});

function connectDB(dbName) {
	db = openDatabase(dbName, '1.0', 'OnlineShopping', 2 * 1024 * 1024);
}

function bindPage(productId) {
	db.transaction(function(tx) {
		tx.executeSql('Select * from PRODUCT where ID = ' + productId + ';', [], function(tx, results) {
			$('#productName').html(results.rows[0].pname);
			$('#productDesc').html(results.rows[0].desc);
			$('#imgProduct').attr('src', '../images/' + results.rows[0].image);
		});
	});
}


function InsertToCart()
{
	db.transaction(function(tx) {
			tx.executeSql('INSERT INTO CART (userId,productId) VALUES ("' + localStorage.getItem("userId") + '","' + productId + '")');
		});
}

function getParameterByName(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};
