var vendorName;
$(document).ready(function() {
	connectDB('OnlineShopping');
	var vendorId = getParameterByName("id");
	bindPage(vendorId);
	$("#productList").attr('href', 'ProductList.html?id=' + vendorId);

});

function connectDB(dbName) {
	db = openDatabase(dbName, '1.0', 'OnlineShopping', 2 * 1024 * 1024);
}

function bindPage(vendorId) {
	db.transaction(function(tx) {
		tx.executeSql('Select * from VENDOR where ID = ' + vendorId + ';', [], function(tx, results) {
			$('#vendorName').html(results.rows[0].VENDOR);
			$('#vendorAddress').html(results.rows[0].ADDRESS);
			$('#vendorPhone').html(results.rows[0].PHONENUMBER);
			$('#monHours').html("Monday : " + results.rows[0].MONDAY);
			$('#tuesHours').html("Tuesday : " + results.rows[0].TUESDAY);
			$('#wedHours').html("Wednesday : " + results.rows[0].WEDNESDAY);
			$('#thurHours').html("Thursday : " + results.rows[0].THURSDAY);
			$('#friHours').html("Friday : " + results.rows[0].FRIDAY);
			$('#satHours').html("Saturday : " + results.rows[0].SATURDAY);
			$('#sunHours').html("Sunday : " + results.rows[0].SUNDAY);
			$('#vendorImage').attr('src', '../images/' + results.rows[0].IMAGEPATH);
		});
	});
}

function getParameterByName(name) {
	if (!url)
		var url = window.location.href;
	var name = name.replace(/[]/g, "\\$&");
	var regex = new RegExp("[?&]" + name + "(=([^]*)|&|#|$)"), results = regex.exec(url);
	if (!results)
		return null;
	if (!results[2])
		return '';

	return decodeURIComponent(results[2].replace(/\+/g, " "));
}
