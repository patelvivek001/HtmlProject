var filePath;

$(document).ready(function() {
	connectDB('OnlineShopping');
	bindVendor();
	$('input[type="file"]').change(function(e) {
		filePath = e.target.files[0].name;
	});
});

function connectDB(dbName) {
	db = openDatabase(dbName, '1.0', 'OnlineShopping', 2 * 1024 * 1024);
}

function btnSave() {

	var db = openDatabase('OnlineShopping', '1.0', 'OnlineShopping', 2 * 1024 * 1024);

	db.transaction(function(tx) {
		tx.executeSql('CREATE TABLE IF NOT EXISTS PRODUCT  (id INTEGER PRIMARY KEY,pname,desc,qty,vendor,image)');
	});

	var txtPname = $("#txtPname").val();
	var txtDesc = $("#txtDesc").val();
	var txtQty = $("#txtQty").val();
	var lstVendor = $('#lstVendor :selected').text();
	var isAdmin = false;

	if (txtPname == "" || txtDesc == "" || txtQty == "" || lstVendor == "") {
		alert("Please Enter all fields");
	} else {
		isAdmin = true;
		db.transaction(function(tx) {
			tx.executeSql('INSERT INTO PRODUCT (pname,desc,qty,vendor,image) VALUES ("' + txtPname + '","' + txtDesc + '","' + txtQty + '","' + lstVendor + '","' + filePath + '")');
		});
		redirectPage('home.html?isAdmin='+isAdmin);
	}
}

function bindVendor() {
	db.transaction(function(tx) { 
		tx.executeSql('Select VENDOR from VENDOR', [], function(tx, results) {
			for (var i = 0; i < results.rows.length; i++) {
				$("#lstVendor").append($("<option></option>").val(results.rows[i].VENDOR).html(results.rows[i].VENDOR));
			}
		});
	});
}

function redirectPage(pageName) {
	window.location.href = pageName;
}